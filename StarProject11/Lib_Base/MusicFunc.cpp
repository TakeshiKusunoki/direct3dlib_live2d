#include "MusicFunc.h"


#include <assert.h>




// チャンクタグ x86はリトルエンディアンである為、逆になります。
const uint32 CHUNK_RIFF_TAG = 'FFIR';
const uint32 CHUNK_FMT_TAG = ' tmf';
const uint32 CHUNK_DATA_TAG = 'atad';






void parseWaveHeader(FILE* fp, WAVEFORMATEX* wfe)
{
	//wfe->;
}

void CALLBACK waveCallback(HWAVEOUT hWave, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2)
{
	if (uMsg == WOM_DONE)
	{
	}
}


bool MusicFunc::WaveLoad(char * filename[])
{

	//
	// ファイルを開く
	//
	UINT dataNum = 0;
	for (dataNum = 0; filename[dataNum] != nullptr; dataNum++)
	{
	}
	DATA_NUM = dataNum;

	fp = new FILE*[DATA_NUM];
	hWaveOut = new HWAVEOUT[DATA_NUM];
	wfe = new WAVEFORMATEX[DATA_NUM];
	whdr = new WAVEHDR*[DATA_NUM];
	for (size_t i = 0; i < DATA_NUM; i++)
	{
		whdr[i] = new WAVEHDR[44100];
	}

	//ファイル名郡取得
	fileNames = filename;



	return true;
}




void MusicFunc::WavePlay(UINT i)
{
	//もうすでに流れてたら流さない
	if (fp[i])
		return;
	fp[i] = fopen(fileNames[i], "rb");
	if (!fp[i])
	{
		assert(!"waveファイルを開くのに失敗しました\n");
		return;
	}
	// ヘッダーの読み込み
	parseWaveHeader(fp[i], &wfe[i]);

	//波形を出力するオーディオデバイスを開く
	waveOutOpen(&hWaveOut[i], WAVE_MAPPER, &wfe[i], (DWORD)waveCallback, (DWORD)0, CALLBACK_FUNCTION);

	// バッファを確保
	for (int i = 0; i < BUFFER_NUM; i++)
	{
	}
	WAVEHDR*  whdr_ = whdr[i];
	// バッファにデータを読み込む

	// 波形データの情報と再生フラグの設定
	/*WAVEHDR  whdr;

	whdr.dwFlags = WHDR_BEGINLOOP | WHDR_ENDLOOP;
	whdr.dwLoops = 1;
	whdr.lpData = (LPSTR)wave;
	whdr.dwBufferLength = waveLength;*/

	// オーディオデバイスのデータブロック(バッファ)を初期化
	waveOutPrepareHeader(hWaveOut[i], whdr_, sizeof(WAVEHDR));

	// 波形データをオーディオデバイスに書き込み。書き込み後、直ちに波形は再生される。
	waveOutWrite(hWaveOut[i], whdr_, sizeof(WAVEHDR));

}

bool MusicFunc::WaveClose(UINT i)
{
	// オーディオデバイスを閉じる
	{
		waveOutClose(hWaveOut[i]);
	}

	// waveデータを解放}
	for (int i = 0; i < BUFFER_NUM; i++)
	{

	}
	// ファイルを閉じる
	if (fp[i])
	{
		fclose(fp[i]);
		fp[i] = nullptr;
	}
	//delete

	return true;
}
