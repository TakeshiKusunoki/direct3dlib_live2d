#include "All.h"
EXTERN MyMesh* Models;
DirectX::XMFLOAT3 Location_[] = {
	//{ 0,0,0 },//中央
	{ 0,0,-100 },//頂上
	{ 100,0,-40 },//左
	{ -100,0,-40 },//右
	{ 70,0,80 },//左下
	{ -70,0,80 },//右下
};
bool shotFallFlag[3] = { false };//玉がおちているか


//******************************************************************************
//
//      ShotShoot
//
//******************************************************************************
void ShotShoot::move(OBJ3D * obj)
{
	switch (obj->state)
	{
		case 0:
			//星発射音
			/*pMusic->soundPlay(14);
			pMusic->soundPlay(2);*/
			pMusic->soundPlay(6);

			switch (obj->playerNum)
			{
				case ENUM::PLAYER_1:
					obj->Model = Models[ENUM::MODEL_DATANUM::STAR];
					break;
				case ENUM::PLAYER_2:
					obj->Model = Models[ENUM::MODEL_DATANUM::STAR_RED];
					break;
				default:
					obj->Model = Models[ENUM::MODEL_DATANUM::CUBE];
					break;
			}
			obj->Axcel._ = { KASOKU,KASOKU,KASOKU };
			obj->Axcel.speedMax = { SPEED_MAX ,SPEED_MAX ,SPEED_MAX };

			obj->timer = 0;
			obj->state++;
		case 1:
			//obj->speed.x += obj->Axcel._.x;
			obj->speed.y += obj->Axcel._.y/10;
			//obj->speed.z += obj->Axcel._.z;

			/*obj->angle.y = ToRadian(obj->angle.y);
			obj->angle.z = ToRadian(1.0f);*/



			ShotShootSppedRelate(obj);
			ShotAreaCheck(obj);

			Relate(obj);
			obj->timer++;
			break;
		default:
			break;
	}
}




//******************************************************************************
//
//      ShotFallTop
//
//******************************************************************************
void ShotFall::move(OBJ3D * obj)
{
	switch (obj->state)
	{
		case 0:
			switch (obj->playerNum)
			{
				case ENUM::PLAYER_1:
					obj->Model = Models[ENUM::MODEL_DATANUM::STAR];
					break;
				case ENUM::PLAYER_2:
					obj->Model = Models[ENUM::MODEL_DATANUM::STAR_RED];
					break;
				default:
					obj->Model = Models[ENUM::MODEL_DATANUM::CUBE];
					break;
			}
			shotFallFlag[obj->playerNum] = true;
			obj->Axcel._ = { 0,KASOKU,0 };
			obj->Axcel.speedMax = { SPEED_MAX ,SPEED_MAX ,SPEED_MAX };

			//落ちる位置
			switch (obj->ShotInfo.statePos)
			{
				case ENUM::SHOT_POS::SHOT_TOP:// 星頂上へ落ちる
					obj->position = Location_[obj->ShotInfo.statePos];
					obj->scale.x /= 1.4f;
					obj->scale.y /= 1.4f;
					obj->scale.z /= 1.4f;
					break;
				case ENUM::SHOT_POS::SHOT_LEFT:// 星の左へ落ちる
					obj->position = Location_[obj->ShotInfo.statePos];
					obj->position.x = 65;
					obj->Axcel._ = { KASOKU / 3,KASOKU,KASOKU };
					obj->scale.x /= 1.2f;
					obj->scale.y /= 1.2f;
					obj->scale.z /= 1.2f;
					break;
				case ENUM::SHOT_POS::SHOT_RIGHT:
					obj->position = Location_[obj->ShotInfo.statePos];
					obj->position.x = -65;
					obj->Axcel._ = { -KASOKU / 3,KASOKU,KASOKU };
					obj->scale.x /= 1.2f;
					obj->scale.y /= 1.2f;
					obj->scale.z /= 1.2f;
					break;
				case ENUM::SHOT_POS::SHOT_LEFT_DOWN:
					obj->position = Location_[obj->ShotInfo.statePos];
					obj->position.x = 140;
					obj->Axcel._ = { -KASOKU / 2,KASOKU,KASOKU };
					//obj->position = DirectX::XMFLOAT3(-70, 0, 80);
					break;
				case ENUM::SHOT_POS::SHOT_RIGHT_DOWN:
					obj->position = Location_[obj->ShotInfo.statePos];
					obj->position.x = -135;
					obj->Axcel._ = { KASOKU / 2,KASOKU,KASOKU };
					//obj->position = DirectX::XMFLOAT3(150, 0, 80);
					break;
				default:
					break;
			}
			//当たり判定用サイズ
			obj->size = { 30,30,30 };

			obj->position.y = 200;
			obj->timer = 0;
			obj->state++;
		case 1:
			switch (obj->ShotInfo.statePos)
			{
				case ENUM::SHOT_POS::SHOT_LEFT_DOWN:
					//obj->speed.x -= obj->Axcel._.x / 14;
					break;
				case ENUM::SHOT_POS::SHOT_RIGHT_DOWN:
					//obj->speed.x -= obj->Axcel._.x / 10;
					break;
				default:
					break;
			}
			obj->speed.x += obj->Axcel._.x / 14;
			obj->speed.y -= obj->Axcel._.y / 20;

			//obj->angle.x += ToRadian(1);

			ShotShootSppedRelate(obj);
			ShotAreaCheck2(obj);
			Relate(obj);
			break;
		default:
			break;
	}
}









//パーティクル
void Particle::move(OBJ3D * obj)
{
	switch (obj->state)
	{
		case 0:
			switch (obj->playerNum)
			{
				case ENUM::PLAYER_1:
					obj->Model = Models[ENUM::MODEL_DATANUM::STAR];
					break;
				case ENUM::PLAYER_2:
					obj->Model = Models[ENUM::MODEL_DATANUM::STAR_RED];
					break;
				default:
					obj->Model = Models[ENUM::MODEL_DATANUM::CUBE];
					break;
			}
			obj->Axcel._ = { KASOKU,KASOKU ,KASOKU };
			obj->Axcel.speedMax = { SPEED_MAX ,SPEED_MAX ,SPEED_MAX };
			obj->angle.y = 90;
			obj->timer = 0;
			obj->state++;
		case 1:
			//obj->speed.x += obj->Axcel._.x;
			{
				int rnd = rand() % 100;
				int rnd2[] = { 1,1,1 };
				for (int i = 0; i <3; i++)
				{
					if(rand()%2==0)
					rnd2[i] *=-1;
				}
				obj->speed.x += (rnd2[1]) * obj->Axcel._.x / rnd;
				obj->speed.y += (rnd2[0]) * obj->Axcel._.y / rnd;
				obj->speed.z += (rnd2[2]) * obj->Axcel._.z / rnd;
			}

			//obj->speed.z += obj->Axcel._.z;

			/*obj->angle.y = ToRadian(obj->angle.y);
			obj->angle.z = ToRadian(1.0f);*/



			ShotShootSppedRelate(obj);
			ParticleAreaCheck(obj);


			Relate(obj);
			obj->timer++;
			break;
		default:
			break;
	}
}





//******************************************************************************
//
//      発射弾消す
//
//******************************************************************************
void EraceShoot::erase(OBJ3D * obj)
{
	//BaseStageManager::SetShot2Add(true);
	MoveAlgHelper* moveAlgHelper[1] = { nullptr };
	shotFallFlag[0] = false;
	shotFallFlag[1] = false;
	shotFallFlag[2] = false;
	switch (obj->playerNum)
	{
		case ENUM::PLAYER_1:
			pShotManager2->addShot(&shotFall, moveAlgHelper, obj->windowNum, obj->playerNum, obj->ShotInfo.statePos, DirectX::XMFLOAT3(2, 2, 2), DirectX::XMFLOAT3(0, 90, 0), obj->position);
			break;
		case ENUM::PLAYER_2:
			pShotManager->addShot(&shotFall, moveAlgHelper, obj->windowNum, obj->playerNum, obj->ShotInfo.statePos, DirectX::XMFLOAT3(2, 2, 2), DirectX::XMFLOAT3(0, 90, 0), obj->position);
			break;
		default:
			break;
	}

	if (obj->helperManager.getList())obj->helperManager.UnInit();
	obj->clear();
}

void EraceShotFallTop::erase(OBJ3D * obj)
{


	if (obj->helperManager.getList())obj->helperManager.UnInit();
	obj->clear();
}


void EraceShotFallEnd::erase(OBJ3D * obj)
{
	pStageManager->endFlag = true;
	MoveAlgHelper* moveAlgHelper[1] = { nullptr };
	//防がれなかった
	if (!obj->Flag.interrapt)
	{
		if (obj->playerNum == ENUM::PLAYER_1)
		{
			for (int i = 0; i < 100; i++)
			{
				pParticle2->add(&particle, moveAlgHelper, obj->position);
			}
		}
		else
		{
			for (int i = 0; i < 100; i++)
			{
				pParticle->add(&particle, moveAlgHelper, obj->position);
			}
		}



		pStageManager->point[obj->playerNum]++;
		if (obj->playerNum == 0)
		{
			//星が穴に入った時の音
			pMusic->soundPlay(3);
			pMusic->soundPlay(10);
			printf("プレイヤーにとくてん%d点\n", pStageManager->point[obj->playerNum]);
		}
		else
		{
			pMusic->soundPlay(5);
			printf("相手にとくてん%d点\n", pStageManager->point[obj->playerNum]);
		}
	}
		//pMusic->musicPlay(ENUM::SOUND);
	if (obj->helperManager.getList())obj->helperManager.UnInit();
	obj->clear();
}







//******************************************************************************
//
//	ShotManager
//
//******************************************************************************
OBJ3D * ShotManager::addShot(MoveAlg * mvAlg, MoveAlgHelper * mvAlgHelper[], UINT windowNum_, int playerNum, ENUM::SHOT_POS shotStatePos, DirectX::XMFLOAT3 scale, DirectX::XMFLOAT3 angle, DirectX::XMFLOAT3 position, bool isAI, DirectX::XMFLOAT4 color)
{
	OBJ3D obj;
	obj.mvAlg = mvAlg;
	//mvAlgHelperは最後にNULLの入った配列
	for (UINT i = 0; mvAlgHelper[i] != nullptr; i++)
	{
		obj.helperManager.add(mvAlgHelper[i], position);
	}

	obj.position = position;
	obj.scale = scale;
	obj.angle.x = angle.x;
	obj.angle.y = angle.y;
	obj.angle.z = angle.z;

	obj.color = color;
	obj.windowNum = windowNum_;
	obj.playerNum = playerNum;
	obj.ShotInfo.statePos = shotStatePos;
	obj.Flag.AI = isAI;
	objList.push_back(obj);//リストに追加
	return &(*objList.rbegin());
}





OBJ3D * ShotManager2::addShot(MoveAlg * mvAlg, MoveAlgHelper * mvAlgHelper[], UINT windowNum_, int playerNum, ENUM::SHOT_POS shotStatePos, DirectX::XMFLOAT3 scale, DirectX::XMFLOAT3 angle, DirectX::XMFLOAT3 position, bool isAI, DirectX::XMFLOAT4 color)
{
	OBJ3D obj;
	obj.mvAlg = mvAlg;
	//mvAlgHelperは最後にNULLの入った配列
	for (UINT i = 0; mvAlgHelper[i] != nullptr; i++)
	{
		obj.helperManager.add(mvAlgHelper[i], position);
	}

	obj.position = position;
	obj.scale = scale;
	obj.angle.x = angle.x;
	obj.angle.y = angle.y;
	obj.angle.z = angle.z;

	obj.color = color;
	obj.windowNum = windowNum_;
	obj.playerNum = playerNum;
	obj.ShotInfo.statePos = shotStatePos;
	obj.Flag.AI = isAI;
	objList.push_back(obj);//リストに追加
	return &(*objList.rbegin());
}














//+ バリア
void Balyer::move(OBJ3D * obj)
{
	switch (obj->state)
	{
		case 0:
			switch (obj->playerNum)
			{
				case ENUM::PLAYER_1:
					obj->Model = Models[ENUM::MODEL_DATANUM::BARIYER1];
					break;
				case ENUM::PLAYER_2:
					obj->Model = Models[ENUM::MODEL_DATANUM::BARIYER2];
					break;
				default:
					obj->Model = Models[ENUM::MODEL_DATANUM::CUBE];
					break;
			}
			//当たり判定用サイズ
			obj->size = { 30,30,30 };
			obj->scale = {3,3,3};

			obj->timer = 0;
			obj->state++;
		case 1:
			obj->angle.x += ToRadian(1);
			obj->angle.z += ToRadian(1);

			Relate(obj);
			obj->timer++;
			break;
		default:
			break;
	}
}







// バリアマネージャー
void BalyerManager::Uninit()
{
	for (auto& it : objList)            // objListの全ての要素をループし、itという名前で各要素にアクセス
	{
		it.eraseAlg = &eraceObj;
	}
}

void BalyerManager2::Uninit()
{
	for (auto& it : objList)            // objListの全ての要素をループし、itという名前で各要素にアクセス
	{
		it.eraseAlg = &eraceObj;
	}
}

void ParticleManager::Uninit()
{
	for (auto& it : objList)            // objListの全ての要素をループし、itという名前で各要素にアクセス
	{
		it.eraseAlg = &eraceObj;
	}
}

void ParticleManager2::Uninit()
{
	for (auto& it : objList)            // objListの全ての要素をループし、itという名前で各要素にアクセス
	{
		it.eraseAlg = &eraceObj;
	}
}
