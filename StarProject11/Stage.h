#pragma once
#include "Common.h"
#include "Lib_3D\\sprite2D.h"

// StageManagerクラス
class BaseStageManager
{
public:
protected:
	int				state;
	int				timer;

	static UINT turn[PLAYER_NUM];//どちらのターンか
	static UINT turnNum;//ターン番号(ターンが変わったら増える)

	DirectX::XMMATRIX view;//ビュー行列
	DirectX::XMMATRIX projection;//プロジェクション行列
	DirectX::XMFLOAT4 light_direction;//光の方向
	DirectX::XMFLOAT4 lightColor;			//光の色
	DirectX::XMFLOAT4 nyutoralLightColor;	//環境光
	float elapsed_time;/*UNIT.23*/
	Sprite2D* spr[100];

public:
	static UINT point[PLAYER_NUM];//獲得ポイント
	static bool endFlag;//終了フラグ
	static bool operationFlag;//操作説明画面表示用
	static int operationCounter;//操作説明閉じる用
public:
	virtual void init() {};
	virtual void update() {};
	virtual void draw() {};

	DirectX::XMFLOAT4 Getlight_direction()
	{
		return light_direction;
	}
	DirectX::XMFLOAT4 GetlightColor()
	{
		return lightColor;
	}
	DirectX::XMFLOAT4 GetnyutoralLightColor()
	{
		return nyutoralLightColor;
	}
	//セッター
	void Setlight_direction(DirectX::XMFLOAT4 x)
	{
		light_direction = x;
	}
	void  SetlightColor(DirectX::XMFLOAT4 x)
	{
		lightColor = x;
	}
	void  SetnyutoralLightColor(DirectX::XMFLOAT4 x)
	{
		nyutoralLightColor = x;
	}
	//ゲッター
	static UINT GetTurn(UINT i = 0)
	{
		return turn[i];
	}
	static UINT GetTurnNum()
	{
		return turnNum;
	}

};


//ウインドウ１
class StageManager : public BaseStageManager, public Singleton<StageManager>
{
public:
	void init();
	void update();
	void draw();
	void uninit();

};
// インスタンス取得
#define	pStageManager		(StageManager::getInstance())

//ウインドウ2
class StageManager2 : public BaseStageManager,public Singleton<StageManager2>
{
public:
	void init();
	void update();
	void draw();
	void uninit();
};
#define	pStageManager2		(StageManager2::getInstance())


//ウインドウ3
class StageManager3 : public BaseStageManager, public Singleton<StageManager3>
{
public:
	void init();
	void update();
	void draw();
	void uninit();
};
#define	pStageManager3		(StageManager3::getInstance())