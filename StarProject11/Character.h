#ifndef INCLUDED_CHARACTER
#define INCLUDED_CHARACTER

//******************************************************************************
//
//
//      Character.h
//
//
//******************************************************************************

//==============================================================================
//
//      移動アルゴリズム
//
//==============================================================================

//ベース(基底クラス)
class CharBase : public Actor
{
public:
	void move(OBJ3D* obj);
protected:
};



//デフォルトキャラクター
class CharDefault : public CharBase
{
public:
	void move(OBJ3D* obj);
protected:
};
EXTERN CharDefault charDefault;


//　デフォルトキャラクター２
class CharSaico : public CharBase
{
public:
	void move(OBJ3D* obj);
protected:
};
EXTERN CharSaico charSaico;

class CharSaicoRed : public CharSaico
{
public:
	void move(OBJ3D* obj);
protected:
};
EXTERN CharSaicoRed charSaicoRed;

class CharSaicoBlue : public CharSaico
{
public:
	void move(OBJ3D* obj);
protected:
};
EXTERN CharSaicoBlue charSaicoBlue;


class CharSaicoYellow : public CharSaico
{
public:
	void move(OBJ3D* obj);
protected:
};
EXTERN CharSaicoYellow charSaicoYellow;










//ベース(基底クラス)
class CharSelectBase : public Actor
{
public:
	void move(OBJ3D* obj);
protected:
};



//　デフォルトキャラクター２
class CharSelect : public CharSelectBase
{
public:
	void move(OBJ3D* obj);
protected:
};
EXTERN CharSelect charSelect;

class CharSelectRed : public CharSelectBase
{
public:
	void move(OBJ3D* obj);
protected:
};
EXTERN CharSelectRed charSelectRed;

class CharSelectBlue : public CharSelectBase
{
public:
	void move(OBJ3D* obj);
protected:
};
EXTERN CharSelectBlue charSelectBlue;


class CharSelectYellow : public CharSelectBase
{
public:
	void move(OBJ3D* obj);
protected:
};
EXTERN CharSelectYellow charSelectYellow;


//==============================================================================
//
//      PlayerManagerクラス
//
//==============================================================================
class PlayerManager : public OBJ3DManager, public Singleton<PlayerManager>
{
public:
};


//==============================================================================
//
//      EnemyManagerクラス
//
//==============================================================================
class EnemyManager : public OBJ3DManager, public Singleton<EnemyManager>
{
public:
};

//------< インスタンス取得 >-----------------------------------------------------
#define pPlayerManager  (PlayerManager::getInstance())
#define PLAYER_BEGIN    (pPlayerManager->getList()->begin())
#define PLAYER_END      (pPlayerManager->getList()->end())


//------< インスタンス取得 >-----------------------------------------------------
#define pEnemyManager  (EnemyManager::getInstance())
#define ENEMY_BEGIN    (pEnemyManager->getList()->begin())
#define ENEMY_END      (pEnemyManager->getList()->end())

//------< ワーク用 >-------------------------------------------------------------

#endif // !INCLUDED_CHARACTER