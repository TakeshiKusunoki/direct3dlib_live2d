#include "All.h"

ENUM::AI selectAILevel = ENUM::AI::AI_EASY;//ＡＩレベル

void SceneSelect::init(UINT i)
{
	timer = 0;
	switch (i)
	{
		case Enum::WINDOW_0://右のウインドウ

			break;
		case Enum::WINDOW_1://左のウインドウ

			break;
		case Enum::WINDOW_2://中央のウインドウ
			spr[0] = new Sprite2D(GetDevice(), L"DATA\\Picture\\Select.png");
			spr[1] = new Sprite2D(GetDevice(), L"DATA\\Picture\\UI_main.png");
			//セレクト画面BGM、キャラクター選択画面BGM
			pMusic->musicPlay(ENUM::SELECT1, true);
				break;
		default:
			break;
	}
}
void SceneSelect::update(UINT i)
{
	static int sceneSelect = 0;
	switch (i)
	{
		case Enum::WINDOW_0://右のウインドウ
			break;
		case Enum::WINDOW_1://左のウインドウ
			break;
		case Enum::WINDOW_2://中央のウインドウ
							//ゲームシーンへ
							//セレクト画面

			//キー移動
			if ((input::TRG(0) & input::PAD_DOWN) || input::GetAsyncKeyTrg(KEY_STATE::KEY_DOWN))
			{
				sceneSelect++;
				if (sceneSelect > 2)
				{
					sceneSelect = 0;
				}
			}
			if (input::TRG(0) & input::PAD_UP || input::GetAsyncKeyTrg(KEY_STATE::KEY_UP))
			{
				sceneSelect--;
				if (sceneSelect < 0)
				{
					sceneSelect = 2;
				}
			}

			switch (sceneSelect)
			{
				case 0:
					selectAILevel = ENUM::AI::AI_EASY;
					break;
				case 1:
					selectAILevel = ENUM::AI::AI_NORMAL;
					break;
				case 2:
					selectAILevel = ENUM::AI::AI_HARD;
					break;
				default:
					break;
			}

			break;
		default:
			break;
	}






	if (timer > SEC)
	{
		if (input::TRG(0) & input::PAD_START || input::TRG(0) & input::PAD_TRG1 || input::TRG(0) & input::PAD_TRG2 || input::TRG(0) & input::PAD_TRG3)
		{
			//音楽をストップさせない

			//セレクト画面から進むSE
			pMusic->soundPlay(1);
			setScene(pSceneCharSelect);
		}
		if (GetAsyncKeyState('Z') < 0 || GetAsyncKeyState('X') < 0 || GetAsyncKeyState('C') < 0 || GetAsyncKeyState(VK_SPACE) < 0)
		{
			//音楽をストップさせない

			//セレクト画面から進むSE
			pMusic->soundPlay(1);
			setScene(pSceneCharSelect);
		}

		//タイトルシーンへ
		if (input::TRG(0) & input::PAD_SELECT)
		{
			//セレクトBGMを止める
			pMusic->musicStop(ENUM::SELECT1);

			setScene(pSceneTitle);
		}
		if (GetAsyncKeyState('Q') < 0)
		{
			//セレクトBGMを止める
			pMusic->musicStop(ENUM::SELECT1);

			setScene(pSceneTitle);
		}


	}
	timer++;
}
void SceneSelect::draw(UINT i)
{
	switch (i)
	{
		case Enum::WINDOW_0://右のウインドウ
			break;
		case Enum::WINDOW_1://左のウインドウ
			break;
		case Enum::WINDOW_2://中央のウインドウ
			spr[0]->Render2(GetDeviceContext(), 0, 0, 0, 0, 1520, 1280, static_cast<float>(WinFunc::GetScreenWidth(i)) / 1520, static_cast<float>(WinFunc::GetScreenHeight(i)) / 1280);

			//SPACEボタン
			spr[1]->Render2(GetDeviceContext(), 1115, 680, 0, 418, 128,64, 0.4f,0.4f);
			switch (selectAILevel)
			{
				case ENUM::AI_EASY://セレクト上
					spr[1]->Render2(GetDeviceContext(), 590, 110, 1504, 418, 240, 100, 1, 1);
					break;
				case ENUM::AI_NORMAL://セレクト真ん中
					spr[1]->Render2(GetDeviceContext(), 805, 360, 1504, 418, 240, 100, 1, 1);
					break;
				case ENUM::AI_HARD://セレクト下
					spr[1]->Render2(GetDeviceContext(), 1000, 595, 1504, 418, 240, 100, 1, 1);
					break;
				default:
					break;
			}

			break;
		default:
			break;
	}
}