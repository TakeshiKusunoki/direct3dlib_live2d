#pragma once
#include	<d3d11.h>
//bool	SetFramework(framework* f);
ID3D11Device*			GetDevice();
ID3D11DeviceContext*	GetDeviceContext();
LONG					GetScreenWidth(UINT i = 0);
LONG					GetScreenHeight(UINT i = 0);
IDXGISwapChain* GetSwapChain(UINT i = 0);
//ID3D11RenderTargetView
ID3D11RenderTargetView* GetRenderTargetView(UINT i = 0);
//ID3D11DepthStencilView*
ID3D11DepthStencilView* GetDepthStencilView(UINT i = 0);

