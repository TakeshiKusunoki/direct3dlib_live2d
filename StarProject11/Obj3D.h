#pragma once
#ifndef INCLUDED_OBJ3D
#define	INCLUDED_OBJ3D
//******************************************************************************
//
//
//      OBJ3Dクラス
//
//
//******************************************************************************




//==============================================================================

// 移動アルゴリズムクラス（抽象クラス）
class MoveAlg
{
public:
	virtual void move(OBJ3D* obj) = 0;  // 純粋仮想関数を持つので、MoveAlg型のオブジェクトは宣言できない（派生させて使う）
};

// 消去アルゴリズムクラス（抽象クラス）
class EraseAlg
{
public:
	virtual void erase(OBJ3D* obj) = 0; // 上と同様
};


//==============================================================================
//
//      OBJ3Dクラス
//
//==============================================================================
class OBJ3D
{
protected:
	//フラグ系
	struct FLAG
	{
		bool active;//活動
		//bool animationEnd;
		bool AI;//AIで動くかどうか?
		bool interrapt;//防がれたか
		bool hadAttacked;//攻撃したか
	};

	// 行動関数のポインタ
	struct BEHAVIER
	{
		void(*waitState)(OBJ3D* obj);//移動状態の関数
		void(*walkState)(OBJ3D* obj);//移動状態の関数
		void(*attackState)(OBJ3D* obj);//攻撃状態の関数
		void(*clashState)(OBJ3D* obj);//クラッシュ状態の関数
		//ステートチェンジでない
	};

	// アニーメーションデータ
	struct MESH_ANIM
	{
		MyMesh Wait;
		MyMesh Walk;
		MyMesh Attack;
		MyMesh Crash;
		void operator=(MyMesh& anm)
		{
			this->Wait = anm;
			this->Walk = anm;
			this->Attack = anm;
			this->Crash = anm;
		};
	};
	// actorparam
	struct MOVE_PARAM
	{
		DirectX::XMFLOAT3 _;                   // 横方向の加速度
		DirectX::XMFLOAT3 speedMax;                // 方向の最大速度
	};

	//ゲーム用オリジナル変数
	struct GAME_METHOD
	{
		BYTE location;//天球上の固定位置
		BYTE prevLocation;//天球上の固定位置,１ｆ前
		BYTE shotLocation;//天球上に星の落ちる固定位置
		BYTE turn;//どっちの番か
		JOYPAD_KEY direction;//振り向き方向
	};

	//Waitステート用変数
	struct WaitStateMetod
	{
		int state;
		int timer;
	};
	//Walkステート用変数
	struct WalkStateMetod
	{
		int state;
		int timer;
	};
	//Attackステート用変数
	struct AttackStateMetod
	{
		int state;
		int timer;
		bool isShoot;//弾がでたか
	};

	// AI用キャラクターstatus情報
	struct AI_CHAR_INFO
	{
		ENUM::AI difficult;//AI難易度
		ENUM::MOVETYPE moveType;//オブジェクト状態
		ENUM::STATE_POS statePos;//天球上の位置
		int AItimer;
		bool ctrl;//キー入力が可能か？
		JOYPAD_KEY direction;//振り向き方向
	};
	// AI用Shot status情報
	struct AI_SHOT_INFO
	{
		ENUM::SHOT_POS statePos;//相手の天球上の位置
	};

	// AIコマンド
	struct AI_COMMAND
	{
		JOYPAD_KEY key;//時計方向
		JOYPAD_KEY stick;//時計方向
		bool a;	bool b;	bool c;//ボタン
		bool shift;	bool space;
		bool operator==(AI_COMMAND com)
		{
			if (this->key == com.key &&
				this->stick == com.stick &&
				this->a == com.a &&
				this->b == com.b &&
				this->c == com.c &&
				this->shift == com.shift &&
				this->space == com.space
				)
			{
				return true;
			}
			return false;
		};
		bool operator!=(AI_COMMAND com)
		{
			if (this->key != com.key ||
				this->stick != com.stick ||
				this->a != com.a ||
				this->b != com.b ||
				this->c != com.c ||
				this->shift != com.shift ||
				this->space != com.space
				)
			{
				return true;
			}
			return false;
		};
	};



public:
	MoveAlg*                mvAlg;              // 移動アルゴリズム
	EraseAlg*               eraseAlg;           // 消去アルゴリズム

	MESH_ANIM Anm;//モデルオブジェクト;(グローバル変数で渡す)
	MyMesh Model;
	DirectX::XMFLOAT3 position;//位置
	DirectX::XMFLOAT3 scale;//大きさ
	DirectX::XMFLOAT3 angle;//回転角度
	DirectX::XMFLOAT4 color;//色
	UINT windowNum;//ウインドウ番号

	int state;
	int timer;

	//当たり判定用サイズ
	DirectX::XMFLOAT3 size;//色

	// ActorState用変数
	DirectX::XMFLOAT3 speed;
	int playerNum;//プレイヤー番号

	// ローカル構造体---------------------------------
	FLAG Flag;//フラグ
	BEHAVIER Behavier;//行動関数のポインタをまとめた構造体
	MOVE_PARAM Axcel;//スピードパラメータをまとめた構造体
	WaitStateMetod stateMethodWait;//Waitステート用変数
	WalkStateMetod stateMethodWalk;//Walkステート用変数
	AttackStateMetod stateMethodAttack;//Attackステート用変数
	AI_CHAR_INFO CharInfo;//AI用キャラクターstatus情報
	AI_SHOT_INFO ShotInfo;//AI用ショット情報
	AI_COMMAND Command;//AIコマンド
	AI_COMMAND PrevCommand;//１フレーム前のAIコマンド
	GAME_METHOD GameMethod;//ゲーム用オリジナル変数
	// -------------------------------------------------
	// 委譲
	ActorState* actorState;
	// マネージャー
	HelperManager		helperManager;	//ヘルパーマネージャー







public:

	OBJ3D();        // コンストラクタ
	void clear();   // メンバ変数のクリア
	void move();    // 移動
	void draw(const DirectX::XMMATRIX & view, const DirectX::XMMATRIX & projection, float elapsed_time);    // 描画
	//void drawHitRect(const VECTOR4&);       // 当たり判定描画
	//bool animeUpdate();    // アニメーションのアップデート
};









//==============================================================================
//
//      消去アルゴリズム
//
//==============================================================================

// 消去アルゴリズム
class EraceObj : public EraseAlg
{
public:
	void erase(OBJ3D* obj); // 上と同様
};

// 消去アルゴリズムの実体
EXTERN EraceObj eraceObj;






//==============================================================================
// OBJ3DManagerクラス
class OBJ3DManager
{
protected:
	std::list<OBJ3D>  objList; // リスト（OBJ3Dの配列のようなもの）
public:
	void init();    // 初期化
	void update();  // 更新
	void draw(const DirectX::XMMATRIX & view,
		const DirectX::XMMATRIX & projection,
		float elapsed_time);    // 描画
	//引数
	//移動関数
	//位置
	//aiフラグ
	 //大きさ
	 //回転角度
	 //色
	OBJ3D* add(MoveAlg* mvAlg,
		MoveAlgHelper* mvAlgHelper[] = nullptr,
		DirectX::XMFLOAT3 position = { 0,0,0 },//位置
		UINT windowNum_ = 0,
		int playerNum = ENUM::PLAYER_1,
		bool isAI = true,
		DirectX::XMFLOAT3 scale = { 1,1,1 },	//大きさ
		DirectX::XMFLOAT3 angle = { 0,0,0 },	//回転角度
		DirectX::XMFLOAT4 color = { 1,1,1,1 }	//色
	); // objListに新たなOBJ3Dを追加する
	std::list<OBJ3D>* getList() { return &objList; }                // objListを取得する
};

//******************************************************************************

#endif// ! INCLUDED_OBJ3D

