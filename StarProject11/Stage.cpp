//******************************************************************************
//
//
//		StageManager
//
//
//******************************************************************************
#include "All.h"
#include "Lib_Base\\winMainFunc.h"
EXTERN int charkeyNum[PLAYERNUM];

EXTERN DirectX::XMFLOAT3 Location[];

//******************************************************************************
//
//		StageManagerクラス
//
//******************************************************************************
UINT BaseStageManager::turn[PLAYER_NUM] = { ENUM::PLAYER_TURN,ENUM::PLAYER_TURN };//どちらのターン
UINT BaseStageManager::turnNum = 0;//ターン番号
UINT BaseStageManager::point[PLAYER_NUM] = { 0,0 };//ポイント
bool BaseStageManager::endFlag = false;//ターン終了フラグ
bool BaseStageManager::operationFlag = false;//操作説明表示用
int  BaseStageManager::operationCounter = 0;//操作説明画面閉じる用



// 初期設定
void StageManager::init()
{
	state = 0;
	timer = 0;
	turn[0] = ENUM::PLAYER_TURN;
	turn[1] = ENUM::PLAYER_TURN;
	turnNum = 0;
	point[0] = 0;
	point[1] = 0;
	operationCounter = 0;
	endFlag = false;
	operationFlag = false;

	spr[0] = new Sprite2D(GetDevice(), L"DATA\\Picture\\UI_main.png");
	spr[1] = new Sprite2D(GetDevice(), L"DATA\\Picture\\setumei.png");
	//バトルシーンBGM
	pMusic->musicPlay(ENUM::BATTLE, true);

	pPlayerManager->init();
	pShotManager->init();
	pBuckManager->init();
	pBalyerManager->init();
	pParticle->init();
}

//==============================================================================

// 更新処理
void StageManager::update()
{
	switch (state)
	{
		case 0://ステージに配置
			//	投影変換行列
			//projection = DirectX::XMMatrixOrthographicLH(16.0f, 9.0f, 0.1f, 1000.0f);
			//	光源(平行光)
			light_direction = DirectX::XMFLOAT4(-300, 500, -3, 0);		//	上+奥 から 下+前へのライト
			lightColor = { 0.995f,0.995f,0.999f,0.99f };
			nyutoralLightColor = { 0.20f,0.21f,0.20f,1 };
			pPlayerManager->init();
			pShotManager->init();
			pBuckManager->init();
			pBalyerManager->init();
			//pParticle->init();
			//キャラ追加
			{
				MoveAlgHelper* moveAlgHelper[1] = { nullptr };
				switch (charkeyNum[ENUM::PLAYER_1])
				{
					case ENUM::CYARACTER_NUM::CYARACTER1:
						pPlayerManager->add(&charDefault, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 0), Enum::WINDOW_0, ENUM::PLAYER_1, false, DirectX::XMFLOAT3(0.5f, 0.5f, 0.5f));
						break;
					case ENUM::CYARACTER_NUM::CYARACTER1RED:
						pPlayerManager->add(&charSaicoRed, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 0), Enum::WINDOW_0, ENUM::PLAYER_1, false, DirectX::XMFLOAT3(0.5f, 0.5f, 0.5f));
						break;
					case ENUM::CYARACTER_NUM::CYARACTER1YELLOW:
						pPlayerManager->add(&charSaicoYellow, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 0), Enum::WINDOW_0, ENUM::PLAYER_1, false, DirectX::XMFLOAT3(0.5f, 0.5f, 0.5f));
						break;
					case ENUM::CYARACTER_NUM::CYARACTER1BLUE:
						pPlayerManager->add(&charSaicoBlue, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 0), Enum::WINDOW_0, ENUM::PLAYER_1, false, DirectX::XMFLOAT3(0.5f, 0.5f, 0.5f));
						break;
					default:
						break;
				}

				/*pPlayerManager->add(&charDefault, moveAlgHelper, DirectX::XMFLOAT3(0, 100, 0), Enum::WINDOW_0, ENUM::PLAYER_2, false);
				pPlayerManager->add(&charDefault, moveAlgHelper, DirectX::XMFLOAT3(-100, 0, 0), Enum::WINDOW_0, ENUM::PLAYER_2, false);
				pPlayerManager->add(&charDefault, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 300), Enum::WINDOW_0, ENUM::PLAYER_NONE, false);*/

			}
			//背景追加
			{
				MoveAlgHelper* moveAlgHelper[1] = { nullptr };
				pBuckManager->add(&Tenkyu, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 0), Enum::WINDOW_0, ENUM::PLAYER_2, false, DirectX::XMFLOAT3(10, 10, 10)/*, DirectX::XMFLOAT3(0, 90, 0)*/);
				pBuckManager->add(&Planet2, moveAlgHelper, DirectX::XMFLOAT3(-250, 0, -50), Enum::WINDOW_0, ENUM::PLAYER_1, false, DirectX::XMFLOAT3(0.05f, 0.05f, 0.05f));
				pBuckManager->add(&Planet3, moveAlgHelper, DirectX::XMFLOAT3(-200, -200, -50), Enum::WINDOW_0, ENUM::PLAYER_1, false, DirectX::XMFLOAT3(0.05f, 0.05f, 0.05f));
				//pBuckManager->add(&Utyu, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 0), Enum::WINDOW_0, ENUM::PLAYER_1, false, DirectX::XMFLOAT3(0.001f, 0.001f, 0.001f));
			}
			//DestroyWindow(WinFunc::GetHwnd(1));
			state++;
		case 1://ゲーム部分

			//光の色
			{
				static float ang = 0;
				light_direction.x = cosf(ToRadian(ang)) * 70;
				light_direction.y = -sinf(ToRadian(ang)) * 70;

				nyutoralLightColor.x = cosf(ToRadian(ang)) / 2 + 0.2f;
				nyutoralLightColor.y = sinf(ToRadian(ang)) / 2 + 0.2f;

				ang += 0.07f;
			}

			//light_direction.z;

			pPlayerManager->update();
			pShotManager->update();
			pBuckManager->update();
			pBalyerManager->update();
			pParticle->update();

			//ターンが変わったら
			if (turn[ENUM::PLAYER_TURN] != ((turnNum+2) % 2))
			{
				printf("turn %d\n", turnNum);
				state++;
			}

			timer++;
			break;
		case 2://ターン遷移初期化
			timer = 0;
			state++;
		case 3://ターン遷移間処理

			pPlayerManager->update();
			pShotManager->update();
			pBuckManager->update();
			pBalyerManager->update();
			pParticle->update();
			//次のターン開始
			if (timer > SEC * 5)
			{
				turn[ENUM::PLAYER_TURN] = turnNum % 2;
				if (turn[ENUM::PLAYER_TURN] == ENUM::PLAYER_TURN)
					printf("自分の番\n");
				/*pMusic->musicPlay(ENUM::SOUND);*/
				state = 0;
			}
			timer++;
			break;
	}



		if (input::GetAsyncKeyTrg(KEY_STATE::KEY_SPACE))
		{
			operationCounter++;
			if (operationCounter % 2 == 0)
			{
				operationFlag = false;
			}
			else {
				operationFlag = true;
			}
		}





}

//==============================================================================


// 描画処理
void StageManager::draw()
{
	// カメラクラス
	Camera camera;
	//	ビュー変換行列
	view = camera.GetView_();

	//	投影変換行列（平行投影）
	LONG width = WinFunc::GetScreenWidth();
	LONG height = WinFunc::GetScreenHeight();
	float aspect = (float)width / height;
	projection = camera.SetPerspective(30 * 0.01745f, aspect, 0.1f, 2000);
	//projection = camera.GetProjection();


	/////////////2dUI描画//////////


	switch (turnNum)
	{
	case 0:
	case 2:
	case 4:
		////自分のターン
	spr[0]->Render2(GetDeviceContext(), 0, 652, 640, 0, 640, 128, 1, 0.9f);
	break;

	}





	// チュートリアル
	static bool once = true;//移動が初めてなら
	static bool onceatk = true;//攻撃が初めてなら
	static bool onceatk2 = true;//攻撃が初めてなら

	if(PLAYER_BEGIN->GameMethod.location != ENUM::STATE_POS::STATE_CENTER)
	{
		once = false;
	}
	if (PLAYER_BEGIN->actorState == pAttackState && !once)
	{
		onceatk = false;
	}
	// 3Dモデル描画
	pPlayerManager->draw(view, projection, pSceneGame->GetTimer());
	pShotManager->draw(view, projection, pSceneGame->GetTimer());
	pParticle->draw(view, projection, pSceneGame->GetTimer());
	pBalyerManager->draw(view, projection, pSceneGame->GetTimer());
	pBuckManager->draw(view, projection, pSceneGame->GetTimer());


	//<ここから下は３Ｄモデルの前に描画>-------------------------------------
	//　中央にいるときの説明
	if (once)
	{
		spr[0]->Render2(GetDeviceContext(), 384, 35, 256, 208, 256, 90, 1, 1);
		/*spr[0]->Render2(GetDeviceContext(), 560, 560, 0, 0, 200, 200);*/
	}
	// 攻撃の説明
	if (onceatk && !once)
	{
		spr[0]->Render2(GetDeviceContext(), 384, 35, 512, 208, 256, 151, 1, 1);
		/*spr[0]->Render2(GetDeviceContext(), 100, 0, 0, 0, 200, 200);*/
	}
	//　	ガードするとゆう説明
	if (pStageManager->GetTurnNum() % 2 == 1)
	{
		spr[0]->Render2(GetDeviceContext(), 384, 35, 768, 208, 257, 210, 1, 1);
		/*spr[0]->Render2(GetDeviceContext(), 400, 0, 0, 0, 200, 200);*/
	}

	//ポイント数字描画
	switch (point[0])
	{
	case 0:
		spr[0]->Render2(GetDeviceContext(), 555, 580, 1352, 128, 70, 80, 0.8f, 0.8f);
		break;
	case 1:
		////ポイントの数字１
		spr[0]->Render2(GetDeviceContext(), 555, 580, 350, 128, 70, 80, 0.8f, 0.8f);
		break;
	case 2:
		////ポイント数字２
		spr[0]->Render2(GetDeviceContext(), 555, 580, 420, 128, 70, 80, 0.8f, 0.8f);
		break;
	case 3:
		////ポイント数字３
		spr[0]->Render2(GetDeviceContext(), 555, 580, 490, 128, 70, 80, 0.8f, 0.8f);
		break;
	case 4:
		spr[0]->Render2(GetDeviceContext(), 555, 580, 1422, 128, 70, 80, 0.8f, 0.8f);
		break;
	case 5:
		spr[0]->Render2(GetDeviceContext(), 555, 580, 1492, 128, 70, 80, 0.8f, 0.8f);
		break;
	case 6:
		spr[0]->Render2(GetDeviceContext(), 555, 580, 1562, 128, 70, 80, 0.8f, 0.8f);
		break;
	}

	////現在のポイント
	spr[0]->Render2(GetDeviceContext(), 364, 610, 560, 128, 276, 40, 1, 1);

	//操作説明画面描画
	if (operationFlag)
	{
		spr[1]->Render2(GetDeviceContext(), 0, 0, 0, 0, 640, 760, 1, 1);
	}

}



void StageManager::uninit()
{
}

//******************************************************************************






















//******************************************************************************
// window2
void StageManager2::init()
{
	state = 0;
	timer = 0;

	spr[0] = new Sprite2D(GetDevice(), L"DATA\\picture\\UI_main.png");
	spr[1] = new Sprite2D(GetDevice(), L"DATA\\picture\\botan.png");

	pShotManager2->init();
	pEnemyManager->init();
	pBuckManager2->init();
	pBalyerManager2->init();
	pParticle2->init();
}



void StageManager2::update()
{
	switch (state)
	{
		case 0://ステージに配置
			   //	投影変換行列
			   //projection = DirectX::XMMatrixOrthographicLH(16.0f, 9.0f, 0.1f, 1000.0f);
			   //	光源(平行光)
			light_direction = DirectX::XMFLOAT4(-300, 500, -3, 0);		//	上+奥 から 下+前へのライト
			lightColor = { 0.995f,0.995f,0.999f,0.99f };
			nyutoralLightColor = { 0.20f,0.21f,0.20f,1 };
			pShotManager2->init();
			pEnemyManager->init();
			pBuckManager2->init();
			pBalyerManager2->init();
			//pParticle2->init();
			//キャラ追加
			{
				MoveAlgHelper* moveAlgHelper[1] = { nullptr };
				switch (charkeyNum[ENUM::PLAYER_2])
				{
					case ENUM::CYARACTER_NUM::CYARACTER1:
						pEnemyManager->add(&charDefault, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 0), Enum::WINDOW_1, ENUM::PLAYER_2, true, DirectX::XMFLOAT3(0.5f, 0.5f, 0.5f));
						break;
					case ENUM::CYARACTER_NUM::CYARACTER1RED:
						pEnemyManager->add(&charSaicoRed, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 0), Enum::WINDOW_1, ENUM::PLAYER_2, true, DirectX::XMFLOAT3(0.5f, 0.5f, 0.5f));
						break;
					case ENUM::CYARACTER_NUM::CYARACTER1YELLOW:
						pEnemyManager->add(&charSaicoYellow, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 0), Enum::WINDOW_1, ENUM::PLAYER_2, true, DirectX::XMFLOAT3(0.5f, 0.5f, 0.5f));
						break;
					case ENUM::CYARACTER_NUM::CYARACTER1BLUE:
						pEnemyManager->add(&charSaicoBlue, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 0), Enum::WINDOW_1, ENUM::PLAYER_2, true, DirectX::XMFLOAT3(0.5f, 0.5f, 0.5f));
						break;
					default:
						break;
				}
				//pEnemyManager->add(&charDefault, moveAlgHelper, DirectX::XMFLOAT3(0, 10, 200), Enum::WINDOW_1, ENUM::PLAYER_NONE, false, DirectX::XMFLOAT3(0.5f, 0.5f, 0.5f));
			}
			//背景
			{
				MoveAlgHelper* moveAlgHelper[1] = { nullptr };
				pBuckManager2->add(&Planet, moveAlgHelper, DirectX::XMFLOAT3(150, 50, 0), Enum::WINDOW_1, ENUM::PLAYER_1, false, DirectX::XMFLOAT3(0.05f, 0.05f, 0.05f));
				pBuckManager2->add(&Tenkyu, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 0), Enum::WINDOW_1, ENUM::PLAYER_1, false, DirectX::XMFLOAT3(10, 10, 10), DirectX::XMFLOAT3(0, 90, 0));
			}

			state++;
		case 1://ゲーム部分

			//光の色
			{
				static float ang = 0;
				light_direction.x = cosf(ToRadian(ang)) * 70;
				light_direction.y = -sinf(ToRadian(ang)) * 70;

				nyutoralLightColor.x = sinf(ToRadian(ang)) / 2 + 0.2f;
				nyutoralLightColor.y = cosf(ToRadian(ang)) / 2 + 0.2f;
				ang += 0.07f;
			}

			pShotManager2->update();
			pEnemyManager->update();
			pBuckManager2->update();
			pBalyerManager2->update();
			pParticle2->update();

			//ターンが変わったら
			if (turn[ENUM::ENEMY_TURN] != ((turnNum + 2) % 2))
			{
				state++;
			}

			timer++;
			break;
		case 2://ターン遷移初期化

			timer = 0;
			state++;
		case 3://ターン遷移間処理

			pEnemyManager->update();
			pShotManager2->update();
			pBuckManager2->update();
			pBalyerManager2->update();
			pParticle2->update();
			//次のターン開始
			if (timer > SEC * 5)
			{
				turn[ENUM::ENEMY_TURN] = turnNum % 2;
				if (turn[ENUM::ENEMY_TURN] == ENUM::ENEMY_TURN)
					printf("相手の番\n");

				if (turnNum >= 6)//６ターン経過で結果シーンへ
				{
					if (point[ENUM::PLAYER_1] > point[ENUM::PLAYER_2])
					{
						pSceneGame->setScene(pSceneClear);//プレイヤー勝利
					}
					else if (point[ENUM::PLAYER_1] == point[ENUM::PLAYER_2])
					{
						pSceneGame->setScene(pSceneDrow);
					}
					else
					{
						pSceneGame->setScene(pSceneOver);//プレイヤー負け
					}
				}

				state = 0;
			}
			timer++;
			break;
	}
}




void StageManager2::draw()
{
	Camera camera;
	//	ビュー変換行列
	//view = camera.GetView_();
	//DirectX::XMVECTOR position = DirectX::XMVectorSet(0.0f, 0.0f, 200.0f, 1.0f);	//	カメラの位置
	//DirectX::XMVECTOR target = DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);		//	カメラの注視点
	//DirectX::XMVECTOR	up = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);		//	上ベクトル（仮）
	//view = DirectX::XMMatrixLookAtLH(position, target, up);

	view = camera.GetView_();
	//	投影変換行列（平行投影）
	LONG width = WinFunc::GetScreenWidth(Enum::WINDOW_1);
	LONG height = WinFunc::GetScreenHeight(Enum::WINDOW_1);
	float aspect = (float)width / height;
	projection = camera.SetPerspective(30 * 0.01745f, aspect, 0.1f, 2000);
	//projection = camera.GetProjection();



	switch (turnNum)
	{
	case 1:
	case 3:
	case 5:
		////相手のターン
		spr[0]->Render2(GetDeviceContext(), 0, 652, 0, 0, 640, 128, 1, 0.9f);
		break;

	}


	pEnemyManager->draw(view, projection, pSceneGame->GetTimer());
	pShotManager2->draw(view, projection, pSceneGame->GetTimer());
	pParticle2->draw(view, projection, pSceneGame->GetTimer());
	pBalyerManager2->draw(view, projection, pSceneGame->GetTimer());
	pBuckManager2->draw(view, projection, pSceneGame->GetTimer());

	//ポイント数字描画
	switch (point[1])
	{
	case 0:
		spr[0]->Render2(GetDeviceContext(), 160, 580, 1352, 128, 70, 80, 0.8f, 0.8f);
		break;
	case 1:
		////ポイント数字１
		spr[0]->Render2(GetDeviceContext(), 160, 580, 350, 128, 70, 80, 0.8f, 0.8f);
		break;
	case 2:
		////ポイント数字２
		spr[0]->Render2(GetDeviceContext(), 160, 580, 420, 128, 70, 80, 0.8f, 0.8f);
		break;
	case 3:
		////ポイント数字３
		spr[0]->Render2(GetDeviceContext(), 160, 580, 490, 128, 70, 80, 0.8f, 0.8f);
		break;
	case 4:
		spr[0]->Render2(GetDeviceContext(), 160, 580, 1422, 128, 70, 80, 0.8f, 0.8f);
		break;
	case 5:
		spr[0]->Render2(GetDeviceContext(), 160, 580, 1492, 128, 70, 80, 0.8f, 0.8f);
		break;
	case 6:
		spr[0]->Render2(GetDeviceContext(), 160, 580, 1562, 128, 70, 80, 0.8f, 0.8f);
		break;

	}

	////現在のポイント
	spr[0]->Render2(GetDeviceContext(), 0, 610, 836, 128, 276, 40, 1, 1);


	/////プレイヤーの攻撃ポイント描画///////
	if (turnNum % 2 == 0)
	{


		if (PLAYER_BEGIN->GameMethod.location)
			switch (PLAYER_BEGIN->GameMethod.location)
			{
				case ENUM::STATE_TOP:
					//spr[1]->Render2(GetDeviceContext(), 40, 200, 0, 0, 100, 100);//左上(z)
					spr[0]->Render2(GetDeviceContext(), 5, 310, 320, 418, 192, 64, 0.5f, 0.5f);
					//spr[1]->Render2(GetDeviceContext(), 260, 100, 0, 0, 100, 100);//頂点(x)
					spr[0]->Render2(GetDeviceContext(), 260, 220, 512, 418, 192, 64, 0.5f, 0.5f);
					//spr[1]->Render2(GetDeviceContext(), 470, 200, 0, 0, 100, 100);//右上(c)
					spr[0]->Render2(GetDeviceContext(), 535, 305, 704, 418, 192, 64, 0.5f, 0.5f);
					break;
				case ENUM::STATE_LEFT:
					//spr[1]->Render2(GetDeviceContext(), 0, 480, 0, 0, 100, 100);//左下(z)
					spr[0]->Render2(GetDeviceContext(), 20, 515, 320, 418, 192, 64, 0.5f, 0.5f);
					//spr[1]->Render2(GetDeviceContext(), 40, 200, 0, 0, 100, 100);//左上(x)
					spr[0]->Render2(GetDeviceContext(), 5, 310, 512, 418, 192, 64, 0.5f, 0.5f);
					//spr[1]->Render2(GetDeviceContext(), 260, 100, 0, 0, 100, 100);////頂点(c)
					spr[0]->Render2(GetDeviceContext(), 260, 220, 704, 418, 192, 64, 0.5f, 0.5f);

					break;
				case ENUM::STATE_RIGHT:
					//spr[1]->Render2(GetDeviceContext(), 260, 100, 0, 0, 100, 100);////頂点(z)
					spr[0]->Render2(GetDeviceContext(), 260, 220, 320, 418, 192, 64, 0.5f, 0.5f);
					//spr[1]->Render2(GetDeviceContext(), 470, 200, 0, 0, 100, 100);//右上(x)
					spr[0]->Render2(GetDeviceContext(), 535, 305, 512, 418, 192, 64, 0.5f, 0.5f);
					//spr[1]->Render2(GetDeviceContext(), 560, 500, 0, 0, 100, 100);//右下(c)
					spr[0]->Render2(GetDeviceContext(), 510, 515, 704, 418, 192, 64, 0.5f, 0.5f);
					break;
				case ENUM::STATE_LEFT_DOWN:
					//spr[1]->Render2(GetDeviceContext(), 560, 500, 0, 0, 100, 100);//右下(z)
					spr[0]->Render2(GetDeviceContext(), 510, 515, 320, 418, 192, 64, 0.5f, 0.5f);
					//spr[1]->Render2(GetDeviceContext(), 0, 500, 0, 0, 100, 100);//左下(x)
					spr[0]->Render2(GetDeviceContext(), 20, 515, 512, 418, 192, 64, 0.5f, 0.5f);
					//spr[1]->Render2(GetDeviceContext(), 40, 200, 0, 0, 100, 100);//左上(c)
					spr[0]->Render2(GetDeviceContext(), 5, 310, 704, 418, 192, 64, 0.5f, 0.5f);
					break;
				case ENUM::STATE_RIGHT_DOWN:
					//spr[1]->Render2(GetDeviceContext(), 470, 200, 0, 0, 100, 100);//右上(z)
					spr[0]->Render2(GetDeviceContext(), 535, 305, 320, 418, 192, 64, 0.5f, 0.5f);
					//spr[1]->Render2(GetDeviceContext(), 560, 500, 0, 0, 100, 100);//右下(x)
					spr[0]->Render2(GetDeviceContext(), 510, 515, 512, 418, 192, 64, 0.5f, 0.5f);
					//spr[1]->Render2(GetDeviceContext(), 0, 500, 0, 0, 100, 100);//左下(c)
					spr[0]->Render2(GetDeviceContext(), 20, 515, 704, 418, 192, 64, 0.5f, 0.5f);
					break;
				default:
					break;
			}

		//説明画面表示
		if (operationFlag)
		{
			spr[1]->Render2(GetDeviceContext(), 0, 0, 0, 0, 640, 760, 1, 1);
		}

		//pEnemyManager->draw(view, projection, pSceneGame->GetTimer());
		//pShotManager2->draw(view, projection, pSceneGame->GetTimer());
		//pParticle2->draw(view, projection, pSceneGame->GetTimer());
		//pBalyerManager2->draw(view, projection, pSceneGame->GetTimer());
		//pBuckManager2->draw(view, projection, pSceneGame->GetTimer());

	}
}

void StageManager2::uninit()
{
}












void StageManager3::init()
{
	state = 0;
	timer = 0;
	pBuckManager3->init();

	for (int i = 0; i < 100; i++)
	{
		spr[i] = nullptr;
	}
	spr[0] = new Sprite2D(GetDevice(), L"DATA\\picture\\hankyu_blue.png");
	spr[1] = new Sprite2D(GetDevice(), L"DATA\\picture\\Select.png");
	spr[2] = new Sprite2D(GetDevice(), L"DATA\\picture\\UI_main.png");

}

void StageManager3::update()
{
	switch (state)
	{
		case 0://ステージに配置
			   //	投影変換行列
			   //projection = DirectX::XMMatrixOrthographicLH(16.0f, 9.0f, 0.1f, 1000.0f);
			   //	光源(平行光)
			light_direction = DirectX::XMFLOAT4(-300, 500, -3, 0);		//	上+奥 から 下+前へのライト
			//light_direction = DirectX::XMFLOAT4(0, -1, 1, 0);		//	上+奥 から 下+前へのライト
			lightColor = { 0.995f,0.995f,0.999f,0.99f };
			nyutoralLightColor = { 0.20f,0.21f,0.20f,1 };
			{
				MoveAlgHelper* moveAlgHelper[1] = { nullptr };
				pBuckManager3->add(&Utyu, moveAlgHelper, DirectX::XMFLOAT3(0, 0, 0), Enum::WINDOW_2, ENUM::PLAYER_2, false, DirectX::XMFLOAT3(0.02f, 0.02f, 0.02f));
			}

			state++;
		case 1://ゲーム部分
			static float ang = 0;
			light_direction.x = cosf(ToRadian(ang)) * 70;
			light_direction.y = -sinf(ToRadian(ang)) * 70;
			ang += 0.07f;
			pBuckManager3->update();

			//当たり判定
			Judge();

			//ターン終了フラグ
			if (endFlag)
			{
				endFlag = false;
				turnNum++;//１ターン経過
			}
			if (turnNum >= 6)//６ターン経過で結果シーンへ
			{
				if (point[ENUM::PLAYER_1] > point[ENUM::PLAYER_2])
				{
					//バトルBGMを止める
					pMusic->musicStop(ENUM::BATTLE);
					pSceneGame->setScene(pSceneClear);//プレイヤー勝利
				}
				else if (point[ENUM::PLAYER_1] == point[ENUM::PLAYER_2])
				{
					//バトルBGMを止める
					pMusic->musicStop(ENUM::BATTLE);
					pSceneGame->setScene(pSceneDrow);//引き分け
				}
				else
				{
					//バトルBGMを止める
					pMusic->musicStop(ENUM::BATTLE);
					pSceneGame->setScene(pSceneOver);//プレイヤー負け
				}
			}

			timer++;
			break;
	}
}

void StageManager3::draw()
{
	// カメラクラス
	Camera camera;
	//	ビュー変換行列
	static DirectX::XMFLOAT3 base(0.0f, 50.0f, 100.0f);
	static DirectX::XMFLOAT3 tBase(0.0f, 0.0f, 0.0f);
	DirectX::XMVECTOR	up;
	DirectX::XMVECTOR position;//位置
	DirectX::XMVECTOR target;//注視点
	//DirectX::XMMATRIX projection;//	投影行列 キャッシュ用

	DirectX::XMFLOAT4 cameraPos;
	cameraPos = { base.x, base.y, base.z, 1.0f };
	position = DirectX::XMLoadFloat4(&cameraPos);

	target = DirectX::XMVectorSet(tBase.x, tBase.y, tBase.z, 1.0f);
	up = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	view = DirectX::XMMatrixLookAtLH(position, target, up);
	//view = camera.GetView_();

	//	投影変換行列（平行投影）
	LONG width = WinFunc::GetScreenWidth(Enum::WINDOW_2);
	LONG height = WinFunc::GetScreenHeight(Enum::WINDOW_2);
	float aspect = (float)width / height;
	projection = camera.SetPerspective(30 * 0.01745f, aspect, 0.1f, 800);

	pBuckManager3->draw(view, projection, pSceneGame->GetTimer());

	///////////2dUI描画//////////////

	////〇〇ターン
	spr[2]->Render2(GetDeviceContext(), 300, 0, 1280, 0, 640, 128, 1, 1);
	//back操作説明
	spr[2]->Render2(GetDeviceContext(), 0, 0, 0, 208, 256, 65, 1, 1);


	switch (turnNum)
	{
	case 0 :
	case 1 :

		//ターン数字１
		spr[2]->Render2(GetDeviceContext(), 460, 15, 0, 128, 70, 80, 1.3f, 1.3f);
		//目
		spr[2]->Render2(GetDeviceContext(), 780, 40, 140, 128, 70, 80, 1, 1);
		break;

\
	case 2:
	case 3:

		//ターン数字２
		spr[2]->Render2(GetDeviceContext(), 460, 15, 70, 128, 70, 80, 1.3f, 1.3f);
		//目
		spr[2]->Render2(GetDeviceContext(), 780, 40, 140, 128, 70, 80, 1, 1);
		break;

	case 4 :
	case 5 :

		//最終
		spr[2]->Render2(GetDeviceContext(), 380, 13, 210, 128, 140, 80, 1.3f, 1.3f);
		break;
	default :

		break;

	}


	//spr[2]->Render2(GetDeviceContext(), 50, 60, 0, 0, 1024, 1024, static_cast<float>(WinFunc::GetScreenWidth(Enum::WINDOW_2)) / 1024, static_cast<float>(WinFunc::GetScreenHeight(Enum::WINDOW_2)) / 1024);
	/*if (GetAsyncKeyState('1'))
	{
		spr[1]->Render2(GetDeviceContext(), 0, 0, 0, 0, 1520, 1280, static_cast<float>(WinFunc::GetScreenWidth(Enum::WINDOW_2)) / 1520, static_cast<float>(WinFunc::GetScreenHeight(Enum::WINDOW_2)) / 1280);
	}*/
}

void StageManager3::uninit()
{
	for (int i = 0; i < 100; i++)
	{
		if (spr[i])delete[] spr[i];
		spr[i] = nullptr;
	}
}












//if (input::TRG(0) &  input::PAD_R1)
//{
//	lightColor.x = (float)(rand() % 50) / 100 + 0.5f;
//	lightColor.y = (float)(rand() % 50) / 100 + 0.5f;
//	lightColor.z = (float)(rand() % 50) / 100 + 0.5f;
//}
//if (input::TRG(0) &  input::PAD_TRG4)
//{
//	nyutoralLightColor.x = (float)(rand() % 4) / 10 + 0.1f;
//	nyutoralLightColor.y = (float)(rand() % 4) / 10 + 0.1f;
//	nyutoralLightColor.z = (float)(rand() % 4) / 10 + 0.1f;
//}
//if (input::TRG(0) &  input::PAD_SELECT)
//{
//	light_direction.x = (float)(rand()) - (float)(rand());
//	light_direction.y = (float)(rand()) - (float)(rand());
//	light_direction.z = (float)(rand()) - (float)(rand());
//}