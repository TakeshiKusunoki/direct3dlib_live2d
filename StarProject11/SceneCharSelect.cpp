#include "All.h"

int charkeyNum[PLAYERNUM] = { 0 };//キャラキー番号
#define CYHAR_MAX 4//キャラクター総数
BYTE flagSelectChyara = ENUM::PLAYER_1;//キャラをセレクトするフラグ

void SceneCharSelect::init(UINT i)
{

	timer = 0;
	for (int i = 0; i < PLAYERNUM; i++)
	{
		state[i] = 0;
		charkeyNum[i] = 0;
	}
	flagSelectChyara = ENUM::PLAYER_1;

	 DirectX::XMFLOAT4 light_direction = DirectX::XMFLOAT4(-300, 500, -3, 0);		//	上+奥 から 下+前へのライト
	DirectX::XMFLOAT4 lightColor = { 0.995f,0.995f,0.999f,0.99f };
	DirectX::XMFLOAT4 nyutoralLightColor = { 0.20f,0.21f,0.20f,1 };
	switch (i)
	{
		case Enum::WINDOW_0://右のウインドウ
			pPlayerManager->init();

			spr[0] = new Sprite2D(GetDevice(), L"DATA\\picture\\UI_main.png");

			pStageManager->SetlightColor(lightColor);
			pStageManager->Setlight_direction(light_direction);
			pStageManager->SetnyutoralLightColor(nyutoralLightColor);
			break;
		case Enum::WINDOW_1://左のウインドウ
			pEnemyManager->init();

			spr[0] = new Sprite2D(GetDevice(), L"DATA\\picture\\UI_main.png");

			pStageManager2->SetlightColor(lightColor);
			pStageManager2->Setlight_direction(light_direction);
			pStageManager2->SetnyutoralLightColor(nyutoralLightColor);
			break;
		case Enum::WINDOW_2://中央のウインドウ
			//spr[0] = new Sprite2D(GetDevice(), L"DATA\\picture\\Select.png");
			spr[1] = new Sprite2D(GetDevice(), L"DATA\\picture\\pikutya.png");
			break;
		default:
			break;
	}
}


#define YPOS -200
void SceneCharSelect::update(UINT i)
{


		MoveAlgHelper* moveAlgHelper[1] = { nullptr };
		float ang = 1;
		switch (i)
		{
			case Enum::WINDOW_0://右のウインドウ
				if (timer > SEC)
				{
					switch (state[i])
					{
						case 0:
							pPlayerManager->init();
							switch (charkeyNum[i])
							{
								case 0:
									pPlayerManager->add(&charSelect, moveAlgHelper, DirectX::XMFLOAT3(0, YPOS, 0), Enum::WINDOW_0, ENUM::PLAYER_1, false, DirectX::XMFLOAT3(ang, ang, ang));

									break;
								case 1:
									pPlayerManager->add(&charSelectRed, moveAlgHelper, DirectX::XMFLOAT3(0, YPOS, 0), Enum::WINDOW_0, ENUM::PLAYER_1, false, DirectX::XMFLOAT3(ang, ang, ang));

									break;
								case 2:
									pPlayerManager->add(&charSelectYellow, moveAlgHelper, DirectX::XMFLOAT3(0, YPOS - 100, 0), Enum::WINDOW_0, ENUM::PLAYER_1, false, DirectX::XMFLOAT3(ang, ang, ang));

									break;
								case 3:
									pPlayerManager->add(&charSelectBlue, moveAlgHelper, DirectX::XMFLOAT3(0, YPOS, 0), Enum::WINDOW_0, ENUM::PLAYER_1, false, DirectX::XMFLOAT3(ang, ang, ang));

									break;
								default:
									break;
							}
							state[i]++;
						case 1:
							pPlayerManager->update();
							if (flagSelectChyara == ENUM::PLAYER_1)
							{
								if (input::TRG(0) & input::PAD_LEFT || input::GetAsyncKeyTrg(KEY_STATE::KEY_LEFT))
								{
									//キャラクター選択の音
									pMusic->soundPlay(13);
									charkeyNum[i]--;//キー後退
									if (charkeyNum[i] < 0)
									{
										charkeyNum[i] = 3;
									}
									state[i]--;
								}
								if (input::TRG(0) & input::PAD_RIGHT || input::GetAsyncKeyTrg(KEY_STATE::KEY_RIGHT))
								{
									//キャラクター選択の音
									pMusic->soundPlay(13);
									charkeyNum[i]++;//キー前進
									if (charkeyNum[i] > 3)
									{
										charkeyNum[i] = 0;
									}
									state[i]--;
								}


								//タイトルシーンへ
								if (input::TRG(0) & input::PAD_SELECT)
								{
									//セレクトBGMを止める
									pMusic->musicStop(ENUM::SELECT1);
									setScene(pSceneTitle);
								}
								if (GetAsyncKeyState('Q') < 0)
								{
									//セレクトBGMを止める
									pMusic->musicStop(ENUM::SELECT1);
									setScene(pSceneTitle);
								}

								if (input::TRG(0) & input::PAD_START || input::TRG(0) & input::PAD_TRG1 || input::TRG(0) & input::PAD_TRG2 || input::TRG(0) & input::PAD_TRG3)
								{
									//キャラクター決定の音
									pMusic->soundPlay(1);
									flagSelectChyara = ENUM::PLAYER_2;
									timer = 0;
								}
								if (GetAsyncKeyState('Z') < 0 || GetAsyncKeyState('X') < 0 || GetAsyncKeyState('C') < 0 || GetAsyncKeyState(VK_SPACE) < 0)
								{
									//キャラクター決定の音
									pMusic->soundPlay(1);
									flagSelectChyara = ENUM::PLAYER_2;
									timer = 0;
								}
							}
							pPlayerManager->update();

							break;
						default:
							break;
					}
				}
				break;
			case Enum::WINDOW_1://左のウインドウ
				if (timer > SEC)
				{
					switch (state[i])
					{
						case 0:
							pEnemyManager->init();
							switch (charkeyNum[i])
							{
								case 0:
									pEnemyManager->add(&charSelect, moveAlgHelper, DirectX::XMFLOAT3(0, YPOS, 0), Enum::WINDOW_1, ENUM::PLAYER_2, false, DirectX::XMFLOAT3(ang, ang, ang));

									break;
								case 1:
									pEnemyManager->add(&charSelectRed, moveAlgHelper, DirectX::XMFLOAT3(0, YPOS, 0), Enum::WINDOW_1, ENUM::PLAYER_2, false, DirectX::XMFLOAT3(ang, ang, ang));

									break;
								case 2:
									pEnemyManager->add(&charSelectYellow, moveAlgHelper, DirectX::XMFLOAT3(0, YPOS - 100, 0), Enum::WINDOW_1, ENUM::PLAYER_2, false, DirectX::XMFLOAT3(ang, ang, ang));

									break;
								case 3:
									pEnemyManager->add(&charSelectBlue, moveAlgHelper, DirectX::XMFLOAT3(0, YPOS, 0), Enum::WINDOW_1, ENUM::PLAYER_2, false, DirectX::XMFLOAT3(ang, ang, ang));


									break;
								default:
									break;
							}
							state[i]++;
						case 1:
							pEnemyManager->update();
							if (flagSelectChyara == ENUM::PLAYER_2)
							{
								if (input::TRG(0) & input::PAD_LEFT || input::GetAsyncKeyTrg(KEY_STATE::KEY_LEFT))
								{
									//キャラクター選択の音
									pMusic->soundPlay(13);
									charkeyNum[i]--;//キー後退
									if (charkeyNum[i] < 0)
									{
										charkeyNum[i] = 3;
									}
									state[i]--;
								}
								if (input::TRG(0) & input::PAD_RIGHT || input::GetAsyncKeyTrg(KEY_STATE::KEY_RIGHT))
								{
									//キャラクター選択の音
									pMusic->soundPlay(13);
									charkeyNum[i]++;//キー前進
									if (charkeyNum[i] > 3)
									{
										charkeyNum[i] = 0;
									}
									state[i]--;
								}
								//タイトルシーンへ
								if (input::TRG(0) & input::PAD_SELECT || input::GetAsyncKeyTrg(KEY_STATE::KEY_SPACE)) {
									timer = 0;
									flagSelectChyara = ENUM::PLAYER_1;
								}
								if (GetAsyncKeyState('Q') < 0) {
									timer = 0;
									flagSelectChyara = ENUM::PLAYER_1;
								}

								if (input::TRG(0) & input::PAD_START || input::TRG(0) & input::PAD_TRG1 || input::TRG(0) & input::PAD_TRG2 || input::TRG(0) & input::PAD_TRG3)
								{
									//キャラクター決定の音
									pMusic->soundPlay(1);
									//セレクトBGMを止める
									pMusic->musicStop(ENUM::SELECT1);
									
									setScene(pSceneGame);
								}
								if (GetAsyncKeyState('Z') < 0 || GetAsyncKeyState('X') < 0 || GetAsyncKeyState('C') < 0 || GetAsyncKeyState(VK_SPACE) < 0)
								{
									//キャラクター決定の音
									pMusic->soundPlay(1);
									//セレウトBGMを止める
									pMusic->musicStop(ENUM::SELECT1);
									setScene(pSceneGame);
								}
							}
							pEnemyManager->update();

							timer++;
							break;
						default:
							break;
					}
					break;
				}
			case Enum::WINDOW_2://中央のウインドウ
								//ゲームシーンへ

				break;
			default:
				break;
		}
	timer++;
}


void SceneCharSelect::draw(UINT i)
{
	// カメラクラス
	Camera camera;
	//	ビュー変換行列
	static DirectX::XMFLOAT3 base(0.0f, 50.0f, 500.0f);
	static DirectX::XMFLOAT3 tBase(0.0f, 0.0f, 0.0f);
	DirectX::XMVECTOR	up;
	DirectX::XMVECTOR position;//位置
	DirectX::XMVECTOR target;//注視点
							 //DirectX::XMMATRIX projection;//	投影行列 キャッシュ用

	DirectX::XMFLOAT4 cameraPos;
	cameraPos = { base.x, base.y, base.z, 1.0f };
	position = DirectX::XMLoadFloat4(&cameraPos);

	target = DirectX::XMVectorSet(tBase.x, tBase.y, tBase.z, 1.0f);
	up = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	DirectX::XMMATRIX view = DirectX::XMMatrixLookAtLH(position, target, up);
	//view = camera.GetView_();

	//	投影変換行列（平行投影）
	LONG width = WinFunc::GetScreenWidth(Enum::WINDOW_2);
	LONG height = WinFunc::GetScreenHeight(Enum::WINDOW_2);
	float aspect = (float)width / height;
	DirectX::XMMATRIX projection = camera.SetPerspective(30 * 0.01745f, aspect, 0.1f, 800);
	switch (i)
	{
		case Enum::WINDOW_0://右のウインドウ
			pPlayerManager->draw(view, projection, pSceneGame->GetTimer());
			if (flagSelectChyara == ENUM::PLAYER_1)
			{
				//キャラクターセレクト文字
				spr[0]->Render2(GetDeviceContext(), 0, 680, 1025, 208, 640, 128, 1,0.8f);
				
				//←やじるし
				spr[0]->Render2(GetDeviceContext(), 0,350 , 992, 418, 256, 192, 0.6f, 0.6f);
				//→やじるし
				spr[0]->Render2(GetDeviceContext(), 484, 350, 1248, 418, 256, 192, 0.6f, 0.6f);
				//SPACEボタン
				spr[0]->Render2(GetDeviceContext(), 422, 616, 0, 418, 128, 64, 1, 1);
				//決定文字
				spr[0]->Render2(GetDeviceContext(), 544, 616, 896, 418, 96, 64, 1, 1);

				/*if (timer & 32 && timer & 9)
					spr[1]->Render2(GetDeviceContext(), 0, 0, 0, 0, 800, 800, static_cast<float>(WinFunc::GetScreenWidth(i)) / 800 / 4, static_cast<float>(WinFunc::GetScreenHeight(i)) / 800/4, 0, 0, 0, 1, 1, 1, 0.3f);*/
			}

			break;
		case Enum::WINDOW_1://左のウインドウ
			pEnemyManager->draw(view, projection, pSceneGame->GetTimer());
			if (flagSelectChyara == ENUM::PLAYER_2)
			{
				//キャラクターセレクト文字
				spr[0]->Render2(GetDeviceContext(), 0, 680, 1025, 208, 640, 128, 1, 0.8f);

				//←やじるし
				spr[0]->Render2(GetDeviceContext(), 0, 350, 992, 418, 256, 192, 0.6f, 0.6f);
				//→やじるし
				spr[0]->Render2(GetDeviceContext(), 484, 350, 1248, 418, 256, 192, 0.6f, 0.6f);

				//SPACEボタン
				spr[0]->Render2(GetDeviceContext(), 0, 616, 0, 418, 128, 64, 1, 1);
				//決定文字
				spr[0]->Render2(GetDeviceContext(), 123, 616, 896, 418, 96, 64, 1, 1);
				/*if (timer& 32 && timer & 9)
				spr[1]->Render2(GetDeviceContext(), 0, 0, 0, 0, 800, 800, static_cast<float>(WinFunc::GetScreenWidth(i)) / 800 / 4, static_cast<float>(WinFunc::GetScreenHeight(i)) / 800/4, 0, 0, 0, 1, 1, 1, 0.3f);*/
			}

			break;
		case Enum::WINDOW_2://中央のウインドウ

			break;
		default:
			break;
	}
}